import './App.css';
import Routes from "./Routes"
import Footer from "./components/Footer/Footer.jsx"

function App() {
  return (
    <div className="App">
        <Routes></Routes>
        <Footer/>
    </div>
  );
}

export default App;
