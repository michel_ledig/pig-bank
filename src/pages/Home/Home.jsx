import React from 'react'
import {Link} from 'react-router-dom'
import "./Home.css"

function Home() {
    return (
        <div>
            <Link to="/cotacao">Entrar na COTACAO</Link>
            <h1>Pig Bank</h1>
            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus. </h3>
            <button>Nova Meta</button>
        </div>
    )
}

export default Home
