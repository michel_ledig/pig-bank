import React from 'react'
import './MoneyCard.css'


function MoneyCard(props) {
    return (
    <div className="cotacao">
        <div className="side-left">
            <h2>{props.dados.name}</h2>
            <p id="Code">{props.dados.code}</p>
        </div>

        <div className="side-right">
            <h1>{`Máxima: R$ ${props.dados.high}`}</h1>
            <h1>{`Mínima: R$ ${props.dados.low}`}</h1>
            <h1 id="data">{`Atualizado em ${props.dados.create_date}`}</h1>
        </div>
    </div> 
    )
}

export default MoneyCard
