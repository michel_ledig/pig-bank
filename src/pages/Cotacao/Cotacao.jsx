import React, { useState, useEffect } from 'react'
import "./Cotacao.css"
import ApiCotacao from "../../services/ApiCotacao.js"
import {Link} from 'react-router-dom'
import MoneyCard from "./MoneyCard/MoneyCard.jsx"
import Header from "../../components/Header/Header.jsx"
import porquinhoLateral from "../../porquinhoLateral.png"

function Cotacao() {
    const [moeda, setMoeda] = useState([])

    useEffect(() => {
        ApiCotacao(setMoeda)
    }, [])

    return (
        <>
            <Link to="/">Entrar na HOME</Link>
            <Header/>
            <div className="CardField">
                {moeda.map(objeto => {
                    return(
                        <MoneyCard dados={objeto}></MoneyCard> 
                    )
                })
            }
            </div>
            <img id="porco1" src={porquinhoLateral}></img>
        </>
)}   

export default Cotacao
