import React from 'react'
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import Cotacao from './pages/Cotacao/Cotacao'
import Home from './pages/Home/Home';
import NotFound from './pages/NotFoud/NotFound'
import "./reset.css"

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home></Home>
                </Route>

                <Route exact path="/cotacao">
                    <Cotacao></Cotacao>
                </Route>

                <Route  path="/">
                    <NotFound></NotFound>
                </Route>
            </Switch>
        
        </BrowserRouter>
    )
}

export default Routes