import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"
})

function ApiCotacao(setEstado){
    api.get("/json/all")
    .then( response => {
        let lista = []
        Object.keys(response.data).forEach(key => {
            lista.push(response.data[key])
            // console.log(Object.keys(response.data))
        })
        setEstado(lista)

    }).catch(error => {
        console.log(error)
    })
}

export default ApiCotacao